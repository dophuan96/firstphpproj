CREATE DATABASE backendUsers
USE backendUsers

CREATE TABLE users (
    id int identity not null,
    lastName varchar(30) null,
    firstName varchar(30) null,
    username varchar(40) not null,
	passwordHash binary(64) not null,
	email varchar(30) null,

	constraint [pkUserTable] primary key (id),
);

