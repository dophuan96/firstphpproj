<?php

/* @var $this yii\web\View */
/* @var $model app\models\LoginForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <div class="body-content">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'action' => ['index'],
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'offset' => 'col-sm-offset-2',
                    'wrapper' => 'col-sm-4',
                ],
            ],
        ]); ?>

        <div class="row">
            <div class="col-sm-4">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>Please fill in this form to sign in:</p>
                <?= $form->field($model, 'username', ["template" => "\n{input}\n{hint}"])
                    ->input('username', ['placeholder' => "Username"])->label(false);?>

                <?= $form->field($model, 'password', ["template" => "\n{input}\n{hint}"])->passwordInput()
                    ->input('password', ['placeholder' => "Password"])->label(false);?>

                <?= $form->field($model, 'rememberMe')->checkbox([
                    'template' => "<div class=\"col-lg-offset-0 col-lg-0\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <a class="btn btn-default" href="index.php?r=site%2Fforgot">Forgot Password?</a>
                </div>

            </div>

            <div class="col-sm-1"> </div>

            <div class="col-sm-7">
                <h1><?= Html::encode("Sign up") ?></h1>
                <p>Sign up for free and always free:</p>
                <div class="row">

                    <div class="col-sm-4">
                        <?= $form->field($model, 'firstname', ["template" => "\n{input}\n{hint}\n{error}"])
                            ->input('firstname', ['placeholder' => "First name"])->label(false);?>

                    </div>

                    <div class="col-sm-1"> </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'lastname', ["template" => "\n{input}\n{hint}\n{error}"])
                            ->input('lastname', ['placeholder' => "Last name"])->label(false);?>
                    </div>

                    <div class="col-sm-9">
                        <?= $form->field($model, 'email', ["template" => "\n{input}\n{hint}\n{error}"])->input('email', ['placeholder' => "Email"])->label(false); ?>
                        <?= $form->field($model, 'user', ["template" => "\n{input}\n{hint}\n{error}"])->input('username', ['placeholder' => "User name"])->label(false); ?>

                    </div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'newpass', ["template" => "\n{input}\n{hint}\n{error}"])->passwordInput()
                            ->input('newpass', ['placeholder' => "New password"])->label(false); ?>

                    </div>

                    <div class="col-sm-1"></div>

                    <div class="col-sm-4">
                        <?= $form->field($model, 'reenterpass', ["template" => "\n{input}\n{hint}\n{error}"])->passwordInput()
                            ->input('reenterpass', ['placeholder' => "Re-enter your password"])->label(false);?>
                    </div>

                    <div class="col-sm-12">
                        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                            'template' => '<div class="row">
                                                <div class="col-lg-6">{image}</div>
                                                <div class="col-lg-6">{input}</div>
                                           </div>',
                            'options' => [
                                'placeholder' => 'Enter verification code',
                                'class' => 'form-control',
                                'style'=>'Top-margin: 10px;'
                            ],
                        ]) ->label(false)?>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-0 col-lg-10">
                            <?= Html::submitButton('Create', ['class' => 'btn btn-primary', 'name' => 'create-button']) ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
</div>
