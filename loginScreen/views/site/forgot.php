<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 8/1/2017
 * Time: 10:24 AM
 */

/* @var $this yii\web\View */
/* @var $model app\models\ForgotPasswordForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Forget password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-forget">
    <div class="body-content">
        <?php $form = ActiveForm::begin([
            'id' => 'forget-form',
            'layout' => 'horizontal',
            'action' => ['index'],
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'offset' => 'col-sm-offset-2',
                    'wrapper' => 'col-sm-4',
                ],
            ],
        ]); ?>
        <div class="col-md-2"></div>
        <div class="row">
            <div class="col-md-8">
                <h3>Find your account:</h3>
                <div class="col-lg-1"></div>

                <div class="col-lg-10">
                    <div class="col-lg-offset-2 col-lg-offset-0">
                        <p>Please enter your email to find your account</p>
                    </div>

                    <?= $form->field($model, 'email', ["template" => "\n{input}\n{hint}"])
                        ->input('email', ['placeholder' => "Your email"])->label(false);?>
                    <div class="form-group">
                        <?= Html::submitButton('Find', ['class' => 'btn btn-primary', 'name' => 'find-button']) ?>
                        <input type="button" value="Cancel" class="btn btn-default btn-cancel">
                    </div>
                </div>

                <div class="col-lg-1"></div>
            </div>
        </div>
        <div class="col-md-2"></div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
