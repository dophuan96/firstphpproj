<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 7/30/2017
 * Time: 11:21 PM
 */

/* @var $this yii\web\View */
/* @var $model app\models\SignUpForm*/
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-signup">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>Sign up for free and always free:</p>

    <div class="body-content">
        <?php $form = ActiveForm::begin([
            'id' => 'signup-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
