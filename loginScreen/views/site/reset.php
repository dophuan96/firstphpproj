<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 8/1/2017
 * Time: 11:00 AM
 */

/* @var $this yii\web\View */
/* @var $model app\models\ResetPass */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset">
    <div class="body-content">
        <?php $form = ActiveForm::begin([
            'id' => 'reset-form',
            'layout' => 'horizontal',
            'action' => ['index'],
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'offset' => 'col-sm-offset-2',
                    'wrapper' => 'col-sm-4',
                ],
            ],
        ]); ?>

        <div class="col-md-2"> </div>
        <div class="col-md-8">

            <div class="col-lg-7">
                <h2>Reset password: </h2>
                <div class="col-lg-2"></div>
                <?= $form->field($model, 'newpass', ["template" => "\n{input}\n{hint}"])
                    ->input('newpass', ['placeholder' => "New Password"])->label(false);?>

                <?= $form->field($model, 'reenterpass', ["template" => "\n{input}\n{hint}"])
                    ->input('reenterpass', ['placeholder' => "Re-enter your password"])->label(false);?>
                <div class="form-group">
                    <?= Html::submitButton('Reset', ['class' => 'btn btn-primary', 'name' => 'find-button']) ?>
                    <input type="button" value="Cancel" class="btn btn-default btn-cancel">
                </div>
            </div>
        </div>
        <div class="col-md-2"> </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
