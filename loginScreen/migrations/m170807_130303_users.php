<?php

use yii\db\Migration;

class m170807_130303_users extends Migration
{
    public function safeUp()
    {
        $this->createTable('users', [
           'id'=>$this->integer(),
            'username'=>$this->string(32)->notNull()->unique(),
            'passHash'=>$this->binary(64)->notNull(),
            'email'=>$this->string(64)->notNull()->unique(),
            'authKey'=>$this->string(50)->notNull()->unique(),
            'accessToken'=>$this->string(50)->notNull()->unique(),
            'firstname'=>$this->string(32),
            'lastname'=>$this->string(32),
            'PRIMARY KEY(id)',
        ]);

        $this->createIndex(
            'idx-usr-id',
            'users',
            'id'
        );
    }

    public function safeDown()
    {
        //
        $this->dropIndex(
            'idx-usr-id',
            'users'
        );

        $this->dropTable('users');
    }


    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170807_130303_users cannot be reverted.\n";

        return false;
    }
    */
}
