<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 8/7/2017
 * Time: 9:02 AM
 */

namespace app\models;
use Yii;

class Login extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'login';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'authKey', 'accessToken', 'email'], 'required'],
            [['username', 'password'], 'string', 'max' => 30],
            [['authKey', 'accessToken'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'authKey' => 'Auth Key',
            'accessToken' => 'Access Token',
            'email'=>'Email',
        ];
    }

}