<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 8/1/2017
 * Time: 9:44 AM
 */

namespace app\models;

use Yii;
use yii\base\Model;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ResetPass extends Model
{
    public $id;
    public $newpass;
    public $reenterpass;

    public function rules()
    {
        return [
            ['newpass','required'],
            ['reenterpass','required'],
            ['reenterpassword', 'compare', 'compareAttribute'=>'newpassword', 'message'=>"Passwords don't match" ],

        ];
    }

}