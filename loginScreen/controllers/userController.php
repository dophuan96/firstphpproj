<?php
/**
 * Created by PhpStorm.
 * User: dophu
 * Date: 8/4/2017
 * Time: 12:32 PM
 */

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
class userController extends Controller
{
    public function behaviors()
    {
        return [
          'verbs' => [
              'class'=>VerbFilter::className(),
              'actions' => [
                  'delete'=>['post'],
              ],
          ],
            'access'=> [
                'class'=>AccessControl::className(),
                'only' => ['create','update', 'delete'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => false,
                        'verbs' => ['post']
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionView($id)
    {
        
    }
}